Rails.application.routes.draw do
  root 'users#index'
  resources :messages
  resources :users
end
