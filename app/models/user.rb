class User < ApplicationRecord
  has_many :sent_messages, class_name: 'Message', foreign_key: 'to_id'
  has_many :received_messages, class_name: 'Message', foreign_key: 'from_id'
  validates :name, presence: true, length: { maximum: 255 }
end
