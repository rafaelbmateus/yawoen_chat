class Message < ApplicationRecord
  belongs_to :from, class_name: 'User'
  belongs_to :to, class_name: 'User'

  validates :body, presence: true, length: { maximum: 280 }
  validate :budget_validation, :on => :create

  private

  def budget_validation
    return unless self.from_id

    user = User.find_by_id(self.from_id)
    if user.budget >= 1
      user.budget -= 1
      user.message_sent_count += 1
      user.save!
    else
      errors.add(:budget, "budget not enough")
    end
  end
end
