require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  before :all do
    5.times do
      User.create(name: 'User', budget: 10.0, message_sent_count: 0)
    end
  end

  let(:valid_attributes) { { from_id: rand(1..5), to_id: rand(1..5), body: 'message body' } }
  let(:invalid_attributes) { { from_id: rand(1..5), to_id: rand(1..5), body: nil } }

  describe "GET #index" do
    it "returns a success response" do
      Message.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      message = Message.create! valid_attributes
      get :show, params: {id: message.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Message" do
        expect {
          post :create, params: {message: valid_attributes}
        }.to change(Message, :count).by(1)
      end

      it "renders a JSON response with the new message" do
        post :create, params: {message: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(message_url(Message.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new message" do
        post :create, params: {message: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { { body: 'new body message' } }

      it "updates the requested message" do
        message = Message.create! valid_attributes
        put :update, params: {id: message.to_param, message: new_attributes}
        message.reload
        expect(message.attributes).to include( { 'body' => 'new body message' } )
      end

      it "renders a JSON response with the message" do
        message = Message.create! valid_attributes

        put :update, params: {id: message.to_param, message: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the message" do
        message = Message.create! valid_attributes

        put :update, params: {id: message.to_param, message: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested message" do
      message = Message.create! valid_attributes
      expect {
        delete :destroy, params: {id: message.to_param}
      }.to change(Message, :count).by(-1)
    end
  end
end
