require 'rails_helper'

RSpec.describe Message, type: :model do
  context 'relationships' do
    it { should validate_presence_of(:body) }
    it { should validate_length_of(:body).is_at_most(280) }
  end

  context 'relationships' do
    it { should belong_to(:from) }
    it { should belong_to(:to) }
  end
end
