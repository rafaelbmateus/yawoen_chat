require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validades' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(255) }
  end

  context 'relationships' do
    it { should have_many(:sent_messages) }
    it { should have_many(:received_messages) }
  end
end
