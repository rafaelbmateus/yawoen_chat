# Yawoen Chat
[![pipeline status](https://gitlab.com/rafaelbmateus/yawoen_chat/badges/master/pipeline.svg)](https://gitlab.com/rafaelbmateus/yawoen_chat/commits/master)

This a project API to Yawoen Company with test suite, database versioning and steps to make your API too.

To build a project is easy. Run with [Docker Compose](#docker-compose).

# Language and framework 
[Ruby](https://www.ruby-lang.org/en/) - 2.5.1 and [Ruby on Rails](https://rubyonrails.org/) - 5.2.3

## System dependencies
* postgresql
* puma
* rspec
* shoulda-matchers

# Development
## <a name="docker-compose"></a>Docker Compose
Don't have [Docker Compose](https://docs.docker.com/compose/)? See the [steps to install](https://docs.docker.com/compose/install/)

* Docker Compose Up
```make docker_server```

* Create Database (run in another terminal, let docker_up running)
```make docker_db```

* Running test suite
```make docker_test```

Point your browser to [http://localhost:3000](http://localhost:3000)

## Do you have Ruby? Nice!
* Rails Server
```make server```

* Create Database
```make db```

* Running test suite
```make test```

Point your browser to [http://localhost:3000](http://localhost:3000)

# GitLab CI/CD Pipeline
I'm using [GitLab CI/CD](https://docs.gitlab.com/ee/ci/yaml/) to Continues and Delivery Integration

# Deployment instructions
git push heroku master

[https://yawoen-chat.herokuapp.com/](https://yawoen-chat.herokuapp.com/)

# Test API
Do you use [Postman](https://www.getpostman.com/)?
See the [wiki](https://gitlab.com/rafaelbmateus/yawoen_chat/wikis/Testing-with-Postman).

## Creating a user
* POST /users
```
{
	"user": {
		"name": "Rafael Mateus"
	}
}
```

## Sent a message
* POST /messages

```
{
	"message": {
		"from_id": 1,
		"to_id": 2,
		"body": "Text body"
	}
}
```