HOST=0.0.0.0
PORT=3000

server:
	rails s -b ${HOST} -p ${PORT}

db:
	rails db:create
	rails db:migrate
	rails db:seed

test:
	rspec

docker_server:
	docker-compose build
	docker-compose up

docker_db:
	docker-compose run web rails db:create
	docker-compose run web rails db:migrate
	docker-compose run web rails db:seed

docker_test:
	docker-compose run web rspec spec