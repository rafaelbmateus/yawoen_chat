class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.decimal :budget
      t.integer :message_sent_count

      t.timestamps
    end
  end
end
